package lui.com.siuguaranimobile.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.LinearGradient;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.ShapeDrawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;
import java.util.Random;

import lui.com.siuguaranimobile.R;
import lui.com.siuguaranimobile.SubjectInfoActivity;
import lui.com.siuguaranimobile.model.SubjectAcademicInfo;

/**
 * Created by luis on 11/11/15.
 */
public class AcademicHistoryElementViewAdapter extends RecyclerView.Adapter<AcademicHistoryElementViewAdapter.DataObjectHolder> {

    private List<SubjectAcademicInfo> subjectInfos;


    public static class DataObjectHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        ImageView thumbnail;
        TextView subjectCode;
        TextView subjectTitle;
        public SubjectAcademicInfo subjectInfo;


        public DataObjectHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            subjectCode = (TextView) itemView.findViewById(R.id.academicSubjectInfoCode);
            subjectTitle = (TextView) itemView.findViewById(R.id.academicSubjectInfoTitle);

            thumbnail = (ImageView) itemView.findViewById(R.id.academicSubjectIcon);
            int[] androidColors = itemView.getResources().getIntArray(R.array.androidcolors);
            int randomAndroidColor = androidColors[new Random().nextInt(androidColors.length)];
            GradientDrawable drawable = (GradientDrawable) thumbnail.getBackground();
            drawable.setColor(randomAndroidColor);

        }

        @Override
        public void onClick(View view) {
            Context context = view.getContext();
            Intent subjectDetailActivity = new Intent(context, SubjectInfoActivity.class);
            subjectDetailActivity.putExtra(SubjectInfoActivity.SUBJECT_INFO, subjectInfo);
            context.startActivity(subjectDetailActivity);
        }
    }

    public AcademicHistoryElementViewAdapter(List<SubjectAcademicInfo> subjectInfos) {
        this.subjectInfos = subjectInfos;
    }

    @Override
    public DataObjectHolder onCreateViewHolder(ViewGroup parent,
                                               int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.academic_history_view_row, parent, false);

        DataObjectHolder dataObjectHolder = new DataObjectHolder(view);
        return dataObjectHolder;
    }

    @Override
    public void onBindViewHolder(DataObjectHolder holder, int position) {
        SubjectAcademicInfo subjectInfo = subjectInfos.get(position);
        holder.subjectTitle.setText(subjectInfo.getSubjectRegularity().getSubject().getName());
        holder.subjectCode.setText(subjectInfo.getSubjectRegularity().getSubject().getId());
        holder.subjectInfo = subjectInfo;
        holder.thumbnail.setImageResource(subjectInfo.getExam() != null ? R.drawable.ic_done_all_white_18dp : R.drawable.ic_done_white_18dp);
    }

    @Override
    public int getItemCount() {
        return subjectInfos.size();
    }

}
