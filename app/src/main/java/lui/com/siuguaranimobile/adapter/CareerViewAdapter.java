package lui.com.siuguaranimobile.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;
import java.util.Random;

import lui.com.siuguaranimobile.AcademicHistoryActivity;
import lui.com.siuguaranimobile.CareerActivity;
import lui.com.siuguaranimobile.R;
import lui.com.siuguaranimobile.model.CareerInscription;
import lui.com.siuguaranimobile.service.CareerService;
import lui.com.siuguaranimobile.service.response.AcademicHistoryResponse;

/**
 * Created by lui on 07/11/15.
 */
public class CareerViewAdapter extends RecyclerView.Adapter<CareerViewAdapter.DataObjectHolder> {

    private List<CareerInscription> careers;
    private CareerService careerService;
    private CareerActivity careerActivity;


    public static class DataObjectHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView careerName;
        TextView careerPlanName;
        View thumbnail;
        CareerInscription inscription;
        private CareerService careerService;
        private Context context;
        private CareerActivity careerActivity;


        public DataObjectHolder(View itemView, CareerService careerService, CareerActivity careerActivity) {
            super(itemView);
            this.careerService = careerService;
            this.careerActivity = careerActivity;
            itemView.setOnClickListener(this);

            careerName = (TextView) itemView.findViewById(R.id.careerName);
            careerPlanName = (TextView) itemView.findViewById(R.id.careerPlanName);
            thumbnail = itemView.findViewById(R.id.careerThumbnail);

            int[] androidColors = itemView.getResources().getIntArray(R.array.androidcolors);
            int randomAndroidColor = androidColors[new Random().nextInt(androidColors.length)];
            thumbnail.setBackgroundColor(randomAndroidColor);
        }

        @Override
        public void onClick(View view) {
            context = view.getContext();
            careerActivity.showProgress(true);
            careerService.setCareerInscription(inscription);
            new GetAcademicHistoryAsyncTask(context).execute();

        }

        private class GetAcademicHistoryAsyncTask extends AsyncTask<Void, Void, AcademicHistoryResponse> {
            private Context context;

            public GetAcademicHistoryAsyncTask(Context context) {
                this.context = context;
            }

            @Override
            protected AcademicHistoryResponse doInBackground(Void... voids) {
                return careerService.getAcademicHistory();
            }

            @Override
            protected void onPostExecute(AcademicHistoryResponse academicHistoryResponse) {
                Intent acHistIntent = new Intent(context, AcademicHistoryActivity.class);
                acHistIntent.putExtra(AcademicHistoryActivity.ACADEMIC_HISTORY_RESPONSE, academicHistoryResponse);
                context.startActivity(acHistIntent);
                int duration = Toast.LENGTH_SHORT;
                Toast toast = Toast.makeText(context, String.format(context.getString(R.string.careerSelectedToast), inscription.getCareer().getName()), duration);
                toast.show();
            }
        }
    }

    public CareerViewAdapter(List<CareerInscription> careers, CareerService careerService, CareerActivity careerActivity) {
        this.careers = careers;
        this.careerService = careerService;
        this.careerActivity = careerActivity;
    }

    @Override
    public DataObjectHolder onCreateViewHolder(ViewGroup parent,
                                               int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.career_card_view_row, parent, false);

        DataObjectHolder dataObjectHolder = new DataObjectHolder(view, careerService, careerActivity);
        return dataObjectHolder;
    }

    @Override
    public void onBindViewHolder(DataObjectHolder holder, int position) {
        CareerInscription inscription = careers.get(position);
        holder.inscription = inscription;
        holder.careerName.setText(inscription.getCareer().getName());
        holder.careerPlanName.setText(inscription.getCareerPlan().getName());
    }

    @Override
    public int getItemCount() {
        return careers.size();
    }


}
