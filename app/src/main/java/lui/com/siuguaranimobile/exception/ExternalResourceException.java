package lui.com.siuguaranimobile.exception;

import lui.com.siuguaranimobile.service.response.ErrorMessage;

/**
 * Created by lui on 07/11/15.
 */
public class ExternalResourceException extends RuntimeException{
    private ErrorMessage errorResponse;

    public ErrorMessage getErrorResponse() {
        return errorResponse;
    }

    public void setErrorResponse(ErrorMessage errorResponse) {
        this.errorResponse = errorResponse;
    }

    public ExternalResourceException(ErrorMessage errorResponse) {
        this.errorResponse = errorResponse;
    }
}
