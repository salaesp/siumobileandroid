package lui.com.siuguaranimobile.client;

import org.springframework.http.HttpBasicAuthentication;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;

/**
 * Created by luis on 10/11/15.
 */
public class HttpRequestBuilder<O> {

    private HttpHeaders headers = new HttpHeaders();
    private HttpMethod method;
    private String url;
    private Class<O> clazz;

    public static HttpRequestBuilder forUrl(String url) {
        HttpRequestBuilder builder = new HttpRequestBuilder();
        builder.url = url;
        builder.headers.setContentType(MediaType.APPLICATION_JSON);
        return builder;
    }

    public HttpRequestBuilder<O> method(HttpMethod method) {
        this.method = method;
        return this;
    }

    public HttpRequestBuilder<O> basicAuthentication(String username, String password) {
        HttpBasicAuthentication authentication = new HttpBasicAuthentication(username, password);
        headers.setAuthorization(authentication);
        return this;
    }

    public HttpRequestBuilder<O> loginKey(String loginKey) {
        headers.set("siuback-loginkey", loginKey);
        return this;
    }

    public HttpRequestBuilder<O> clazz(Class<O> clazz){
        this.clazz = clazz;
        return this;
    }

    public HttpRequest build() {
        HttpEntity<String> request = new HttpEntity<>(headers);
        HttpRequest httpCall = new HttpRequest(url, method, request, clazz);
        return httpCall;
    }

}
