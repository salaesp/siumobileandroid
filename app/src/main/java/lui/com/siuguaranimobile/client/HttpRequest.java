package lui.com.siuguaranimobile.client;

import android.os.AsyncTask;
import android.util.Log;

import org.springframework.http.HttpBasicAuthentication;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import lui.com.siuguaranimobile.service.response.AbstractResponse;

/**
 * Created by lui on 07/11/15.
 */
public class HttpRequest<O extends AbstractResponse> {


    private String url;
    private HttpMethod method;
    private HttpEntity<String> request;
    private Class<O> clazz;

    public HttpRequest(String url, HttpMethod method, HttpEntity<String> request, Class<O> clazz) {
        this.url = url;
        this.method = method;
        this.request = request;
        this.clazz = clazz;
    }

    public O execute() {
        try {
            RestTemplate restTemplate = new RestTemplate();
            ResponseEntity<O> response = restTemplate.exchange(url, method, request, clazz);
            return response.getBody();
        } catch (HttpClientErrorException e) {
            Log.i("HttpRequest", e.getMessage(), e);
        } catch (Exception e) {
            Log.e("HttpRequest", e.getMessage(), e);
        }
        return null;
    }


}
