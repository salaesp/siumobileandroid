package lui.com.siuguaranimobile.service.response;

/**
 * Created by lui on 07/11/15.
 */
public class LoginResponse extends AbstractResponse{
    private String status;
    private String loginKey;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getLoginKey() {
        return loginKey;
    }

    public void setLoginKey(String loginKey) {
        this.loginKey = loginKey;
    }
}
