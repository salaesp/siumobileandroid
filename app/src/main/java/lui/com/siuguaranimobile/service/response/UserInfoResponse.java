package lui.com.siuguaranimobile.service.response;

import java.util.List;

import lui.com.siuguaranimobile.model.CareerInscription;

/**
 * Created by lui on 07/11/15.
 */
public class UserInfoResponse extends AbstractResponse {
    private List<CareerInscription> careers;
    private String username;

    public List<CareerInscription> getCareers() {
        return careers;
    }

    public void setCareers(List<CareerInscription> careers) {
        this.careers = careers;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}