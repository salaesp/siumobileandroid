package lui.com.siuguaranimobile.service;

import com.google.inject.Singleton;

import org.springframework.http.HttpMethod;

import lui.com.siuguaranimobile.R;
import lui.com.siuguaranimobile.client.HttpRequestBuilder;
import lui.com.siuguaranimobile.service.response.UserInfoResponse;
import lui.com.siuguaranimobile.service.response.LoginResponse;
import lui.com.siuguaranimobile.client.HttpRequest;
import roboguice.inject.InjectResource;

/**
 * Created by lui on 07/11/15.
 */
@Singleton
public class UserService {

    @InjectResource(R.string.loginurl)
    private String loginUrl;
    @InjectResource(R.string.meurl)
    private String meUrl;

    private String loginKey;
    private String username;

    public LoginResponse login(String username, String password) {
        HttpRequest<LoginResponse> httpCall = HttpRequestBuilder.forUrl(loginUrl).clazz(LoginResponse.class).method(HttpMethod.GET).basicAuthentication(username, password).build();
        try {
            LoginResponse login = httpCall.execute();
            if (login != null) {
                this.setLoginKey(login.getLoginKey());
            }
            return login;
        } catch (Exception e) {
            return null;
        }
    }

    public UserInfoResponse getUserInfo() {
        HttpRequest<UserInfoResponse> httpCall = HttpRequestBuilder.forUrl(meUrl).clazz(UserInfoResponse.class).method(HttpMethod.GET).loginKey(loginKey).build();
        try {
            UserInfoResponse userInfoResponse = httpCall.execute();
            if(userInfoResponse != null){
                this.setUsername(userInfoResponse.getUsername());
            }
            return userInfoResponse;
        } catch (Exception e) {
            return null;
        }
    }

    public String getLoginKey() {
        return loginKey;
    }

    public void setLoginKey(String loginKey) {
        this.loginKey = loginKey;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
