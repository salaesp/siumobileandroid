package lui.com.siuguaranimobile.service.response;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by luis on 10/11/15.
 */
public class AbstractResponse implements Parcelable {

    private ErrorMessage error;

    public ErrorMessage getError() {
        return error;
    }

    public void setError(ErrorMessage error) {
        this.error = error;
    }


    public boolean hasError(){
        return error != null;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(this.error, flags);
    }

    public AbstractResponse() {
    }

    protected AbstractResponse(Parcel in) {
        this.error = in.readParcelable(ErrorMessage.class.getClassLoader());
    }

}
