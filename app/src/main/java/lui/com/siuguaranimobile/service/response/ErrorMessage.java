package lui.com.siuguaranimobile.service.response;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by lui on 07/11/15.
 */
public class ErrorMessage implements Parcelable {
    private String message;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.message);
    }

    public ErrorMessage() {
    }

    protected ErrorMessage(Parcel in) {
        this.message = in.readString();
    }

    public static final Parcelable.Creator<ErrorMessage> CREATOR = new Parcelable.Creator<ErrorMessage>() {
        public ErrorMessage createFromParcel(Parcel source) {
            return new ErrorMessage(source);
        }

        public ErrorMessage[] newArray(int size) {
            return new ErrorMessage[size];
        }
    };
}
