package lui.com.siuguaranimobile.service.response;

import android.os.Parcel;

import java.util.List;

import lui.com.siuguaranimobile.model.SubjectAcademicInfo;

/**
 * Created by luis on 11/11/15.
 */
public class AcademicHistoryResponse extends AbstractResponse {
    private Double examPercentage;
    private Double subjectsPercentage;
    private Double fullPercentage;
    private List<SubjectAcademicInfo> subjects;

    public Double getSubjectsPercentage() {
        return subjectsPercentage;
    }

    public void setSubjectsPercentage(Double subjectsPercentage) {
        this.subjectsPercentage = subjectsPercentage;
    }

    public Double getExamPercentage() {
        return examPercentage;
    }

    public void setExamPercentage(Double examPercentage) {
        this.examPercentage = examPercentage;
    }

    public Double getFullPercentage() {
        return fullPercentage;
    }

    public void setFullPercentage(Double fullPercentage) {
        this.fullPercentage = fullPercentage;
    }

    public List<SubjectAcademicInfo> getSubjects() {
        return subjects;
    }

    public void setSubjects(List<SubjectAcademicInfo> subjects) {
        this.subjects = subjects;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeValue(this.examPercentage);
        dest.writeValue(this.subjectsPercentage);
        dest.writeValue(this.fullPercentage);
        dest.writeTypedList(subjects);
    }

    public AcademicHistoryResponse() {
    }

    protected AcademicHistoryResponse(Parcel in) {
        super(in);
        this.examPercentage = (Double) in.readValue(Double.class.getClassLoader());
        this.subjectsPercentage = (Double) in.readValue(Double.class.getClassLoader());
        this.fullPercentage = (Double) in.readValue(Double.class.getClassLoader());
        this.subjects = in.createTypedArrayList(SubjectAcademicInfo.CREATOR);
    }

    public static final Creator<AcademicHistoryResponse> CREATOR = new Creator<AcademicHistoryResponse>() {
        public AcademicHistoryResponse createFromParcel(Parcel source) {
            return new AcademicHistoryResponse(source);
        }

        public AcademicHistoryResponse[] newArray(int size) {
            return new AcademicHistoryResponse[size];
        }
    };
}
