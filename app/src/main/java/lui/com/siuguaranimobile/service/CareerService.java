package lui.com.siuguaranimobile.service;

import com.google.inject.Inject;
import com.google.inject.Singleton;

import org.springframework.http.HttpMethod;

import lui.com.siuguaranimobile.R;
import lui.com.siuguaranimobile.client.HttpRequest;
import lui.com.siuguaranimobile.client.HttpRequestBuilder;
import lui.com.siuguaranimobile.model.CareerInscription;
import lui.com.siuguaranimobile.service.response.AcademicHistoryResponse;
import roboguice.inject.InjectResource;

/**
 * Created by luis on 11/11/15.
 */
@Singleton
public class CareerService {

    @InjectResource(R.string.academicHistoryUrl)
    private String academicHistoryUrl;
    @Inject
    private UserService userService;

    private CareerInscription careerInscription;

    public AcademicHistoryResponse getAcademicHistory() {
        String formattedUrl = String.format(academicHistoryUrl, careerInscription.getAcademicUnit().getId(), careerInscription.getCareer().getId(), careerInscription.getCareerPlan().getId());
        HttpRequestBuilder<AcademicHistoryResponse> httpRequestBuilder = HttpRequestBuilder.forUrl(formattedUrl).clazz(AcademicHistoryResponse.class);
        httpRequestBuilder.method(HttpMethod.GET).loginKey(userService.getLoginKey());
        HttpRequest<AcademicHistoryResponse> httpRequest = httpRequestBuilder.build();
        try {
            return httpRequest.execute();
        } catch (Exception e) {
            return null;
        }
    }

    public CareerInscription getCareerInscription() {
        return careerInscription;
    }

    public void setCareerInscription(CareerInscription careerInscription) {
        this.careerInscription = careerInscription;
    }
}
