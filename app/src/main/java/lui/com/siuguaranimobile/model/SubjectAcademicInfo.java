package lui.com.siuguaranimobile.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by luis on 11/11/15.
 */
public class SubjectAcademicInfo implements Parcelable {

    private Exam exam;

    private Regularity subjectRegularity;

    public Exam getExam() {
        return exam;
    }

    public void setExam(Exam exam) {
        this.exam = exam;
    }

    public Regularity getSubjectRegularity() {
        return subjectRegularity;
    }

    public void setSubjectRegularity(Regularity subjectRegularity) {
        this.subjectRegularity = subjectRegularity;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(this.exam, flags);
        dest.writeParcelable(this.subjectRegularity, flags);
    }

    public SubjectAcademicInfo() {
    }

    protected SubjectAcademicInfo(Parcel in) {
        this.exam = in.readParcelable(Exam.class.getClassLoader());
        this.subjectRegularity = in.readParcelable(Regularity.class.getClassLoader());
    }

    public static final Parcelable.Creator<SubjectAcademicInfo> CREATOR = new Parcelable.Creator<SubjectAcademicInfo>() {
        public SubjectAcademicInfo createFromParcel(Parcel source) {
            return new SubjectAcademicInfo(source);
        }

        public SubjectAcademicInfo[] newArray(int size) {
            return new SubjectAcademicInfo[size];
        }
    };
}
