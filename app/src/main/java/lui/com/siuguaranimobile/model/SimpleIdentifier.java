package lui.com.siuguaranimobile.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;

/**
 * Created by lui on 07/11/15.
 */
public class SimpleIdentifier implements Serializable, Parcelable {
    private String name;
    private String id;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.name);
        dest.writeString(this.id);
    }

    public SimpleIdentifier() {
    }

    protected SimpleIdentifier(Parcel in) {
        this.name = in.readString();
        this.id = in.readString();
    }

    public static final Parcelable.Creator<SimpleIdentifier> CREATOR = new Parcelable.Creator<SimpleIdentifier>() {
        public SimpleIdentifier createFromParcel(Parcel source) {
            return new SimpleIdentifier(source);
        }

        public SimpleIdentifier[] newArray(int size) {
            return new SimpleIdentifier[size];
        }
    };
}
