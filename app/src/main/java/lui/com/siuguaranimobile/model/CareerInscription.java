package lui.com.siuguaranimobile.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by lui on 07/11/15.
 */
public class CareerInscription implements Parcelable {
    private SimpleIdentifier careerPlan;
    private SimpleIdentifier career;
    private SimpleIdentifier academicUnit;

    public SimpleIdentifier getCareerPlan() {
        return careerPlan;
    }

    public void setCareerPlan(SimpleIdentifier careerPlan) {
        this.careerPlan = careerPlan;
    }

    public SimpleIdentifier getCareer() {
        return career;
    }

    public void setCareer(SimpleIdentifier career) {
        this.career = career;
    }

    public SimpleIdentifier getAcademicUnit() {
        return academicUnit;
    }

    public void setAcademicUnit(SimpleIdentifier academicUnit) {
        this.academicUnit = academicUnit;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(this.careerPlan, 0);
        dest.writeParcelable(this.career, 0);
        dest.writeParcelable(this.academicUnit, 0);
    }

    public CareerInscription() {
    }

    protected CareerInscription(Parcel in) {
        this.careerPlan = in.readParcelable(SimpleIdentifier.class.getClassLoader());
        this.career = in.readParcelable(SimpleIdentifier.class.getClassLoader());
        this.academicUnit = in.readParcelable(SimpleIdentifier.class.getClassLoader());
    }

    public static final Parcelable.Creator<CareerInscription> CREATOR = new Parcelable.Creator<CareerInscription>() {
        public CareerInscription createFromParcel(Parcel source) {
            return new CareerInscription(source);
        }

        public CareerInscription[] newArray(int size) {
            return new CareerInscription[size];
        }
    };
}

