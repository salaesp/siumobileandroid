package lui.com.siuguaranimobile.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by luis on 11/11/15.
 */
public class Exam extends Regularity {
    private Double grade;

    public Double getGrade() {
        return grade;
    }

    public void setGrade(Double grade) {
        this.grade = grade;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeValue(this.grade);
    }

    public Exam() {
    }

    protected Exam(Parcel in) {
        super(in);
        this.grade = (Double) in.readValue(Double.class.getClassLoader());
    }

    public static final Creator<Exam> CREATOR = new Creator<Exam>() {
        public Exam createFromParcel(Parcel source) {
            return new Exam(source);
        }

        public Exam[] newArray(int size) {
            return new Exam[size];
        }
    };
}
