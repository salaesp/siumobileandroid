package lui.com.siuguaranimobile.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.Date;

/**
 * Created by luis on 11/11/15.
 */
public class Regularity implements Parcelable {

    private Date date;
    private String id;
    private String name;
    private Subject subject;


    public static final Creator<Regularity> CREATOR = new Creator<Regularity>() {
        @Override
        public Regularity createFromParcel(Parcel in) {
            return new Regularity(in);
        }

        @Override
        public Regularity[] newArray(int size) {
            return new Regularity[size];
        }
    };

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Subject getSubject() {
        return subject;
    }

    public void setSubject(Subject subject) {
        this.subject = subject;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(date != null ? date.getTime() : -1);
        dest.writeString(this.id);
        dest.writeString(this.name);
        dest.writeParcelable(this.subject, 0);
    }

    public Regularity() {
    }

    protected Regularity(Parcel in) {
        long tmpDate = in.readLong();
        this.date = tmpDate == -1 ? null : new Date(tmpDate);
        this.id = in.readString();
        this.name = in.readString();
        this.subject = in.readParcelable(Subject.class.getClassLoader());
    }

}
