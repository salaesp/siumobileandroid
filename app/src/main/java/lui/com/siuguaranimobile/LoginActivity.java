package lui.com.siuguaranimobile;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.inject.Inject;

import org.roboguice.shaded.goole.common.collect.Lists;

import java.util.ArrayList;
import java.util.List;

import lui.com.siuguaranimobile.model.CareerInscription;
import lui.com.siuguaranimobile.service.UserService;
import lui.com.siuguaranimobile.service.response.UserInfoResponse;
import lui.com.siuguaranimobile.service.response.LoginResponse;
import roboguice.activity.RoboActionBarActivity;
import roboguice.inject.ContentView;
import roboguice.inject.InjectView;

@ContentView(R.layout.activity_login)
public class LoginActivity extends RoboActionBarActivity {

    public static final String CAREERINSCRIPTIONS = "careerinscriptions";
    @InjectView(R.id.login_progress)
    private View mProgressView;
    @InjectView(R.id.sign_in_button)
    private Button loginButton;

    @InjectView(R.id.password)
    private TextView password;
    @InjectView(R.id.username)
    private TextView username;

    @Inject
    private UserService userService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public void loginAction(View view) throws InterruptedException {
        final String username = this.username.getText().toString();
        if (username.isEmpty()) {
            this.username.setError(getString(R.string.emptyUsername));
            return;
        }
        final String password = this.password.getText().toString();
        if (password.isEmpty()) {
            this.password.setError(getString(R.string.emptyPass));
            return;
        }
        showProgress(true);
        LoginAsyncTask loginAsyncTask = new LoginAsyncTask(username, password, this);
        loginAsyncTask.execute();
    }

    @Override
    protected void onResume() {
        super.onResume();
        showProgress(false);
    }

    private class LoginAsyncTask extends AsyncTask<Void, Void, LoginResponse> {

        private String usernameVal;
        private String passwordVal;
        private Context context;

        private LoginAsyncTask(String usernameVal, String passwordVal, Context context) {
            this.usernameVal = usernameVal;
            this.passwordVal = passwordVal;
            this.context = context;
        }

        @Override
        protected LoginResponse doInBackground(Void... voids) {
            return userService.login(usernameVal, passwordVal);
        }

        @Override
        protected void onPostExecute(LoginResponse loginResponse) {
            if (loginResponse == null) {
                username.setError(getString(R.string.incorrectLogin));
                showProgress(false);
            } else {
                GetCareersAsyncTask careerTask = new GetCareersAsyncTask(context);
                careerTask.execute();
            }
        }
    }


    private class GetCareersAsyncTask extends AsyncTask<Void, Void, List<CareerInscription>> {
        private Context context;

        public GetCareersAsyncTask(Context context) {
            this.context = context;
        }

        @Override
        protected List<CareerInscription> doInBackground(Void... voids) {
            UserInfoResponse userInfoResponse = userService.getUserInfo();
            return userInfoResponse != null ? (ArrayList) userInfoResponse.getCareers() : Lists.<CareerInscription>newArrayList();
        }

        @Override
        protected void onPostExecute(List<CareerInscription> inscriptions) {
            Intent intent = new Intent(context, CareerActivity.class);
            Bundle data = new Bundle();
            data.putParcelableArrayList(CAREERINSCRIPTIONS, (ArrayList)inscriptions);
            intent.putExtra("data", data);
            startActivity(intent);
        }
    }


    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        loginButton.setEnabled(!show);
        password.setEnabled(!show);
        username.setEnabled(!show);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
        }
    }

}
