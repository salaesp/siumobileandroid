package lui.com.siuguaranimobile;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.inject.Key;

import java.text.SimpleDateFormat;

import lui.com.siuguaranimobile.R;
import lui.com.siuguaranimobile.model.Exam;
import lui.com.siuguaranimobile.model.SubjectAcademicInfo;
import lui.com.siuguaranimobile.service.response.AcademicHistoryResponse;
import roboguice.activity.RoboActionBarActivity;
import roboguice.inject.InjectView;

public class SubjectInfoActivity extends RoboActionBarActivity {

    public static final String SUBJECT_INFO = "subject_info";

    @InjectView(R.id.subjectGrade)
    private TextView subjectGrade;
    @InjectView(R.id.subjectCompleteName)
    private TextView subjectCompleteName;
    @InjectView(R.id.subjectRegDate)
    private TextView subjectRegDate;

    @InjectView(R.id.subjectExamDate)
    private TextView examDate;
    @InjectView(R.id.subjectExamInfo)
    private RelativeLayout examLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_subject_info);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        SubjectAcademicInfo subjectInfo = this.getIntent().getParcelableExtra(SUBJECT_INFO);
        setTitle(subjectInfo.getSubjectRegularity().getSubject().getId());

        Exam exam = subjectInfo.getExam();
        if(exam != null){
            subjectGrade.setText(String.valueOf(exam.getGrade().intValue()));
            String examDateFormatted = new SimpleDateFormat(getString(R.string.dateFormat)).format(exam.getDate());
            examDate.setText(examDateFormatted);
        }
        else {
            subjectGrade.setVisibility(View.GONE);
            examLayout.setVisibility(View.GONE);
        }

        subjectCompleteName.setText(subjectInfo.getSubjectRegularity().getSubject().getName());
        String regFormatted = new SimpleDateFormat(getString(R.string.dateFormat)).format(subjectInfo.getSubjectRegularity().getDate());
        subjectRegDate.setText(regFormatted);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }
}
